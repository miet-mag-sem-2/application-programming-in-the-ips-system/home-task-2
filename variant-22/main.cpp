#include <iostream>
#include <vector>
#include <cstdint>
#include <chrono>
#include <thread>

//#define _USE_MATH_DEFINES
#define M_PI 3.141592653589793238462643383279502884197169399375105820974944
#include <math.h>


using namespace std;

// ips home-task-1 variant 22
// Simpsons method
//    /1      4
//   |   ----------- dx
//  /0     1 + x^2

double get_f(double x) {
	return 4 / (1 + x * x);
}

double get_x(double a, double j, double h) {
	return a + j * h;
}

chrono::duration<double> do_the_thing_usual(uint32_t N) {
	double a = 0, b = 1;
	double h = (b - a) / (N);
	double sum = 0;

	double* xs = new double[N];
	double* fs = new double[N];

	chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();

	xs[0] = a;
	for (int i = 1; i < N; i++)
		xs[i] = xs[i - 1] + h;

	for (int i = 0; i < N; i++)
		fs[i] = get_f(xs[i]);
	
	for (int i = 1; i <= N - 1; i++) {
		//sum += ((i % 2) ? 4 : 2) * get_f(xs[i]);
		sum += ((i % 2) ? 4 : 2) * fs[i];
	}

	chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
	chrono::duration<double> duration = (t2 - t1);

	double result = h / 3 * sum;
	double calc_error = abs(result - M_PI);

	printf("N = %d\nIdeal result = \t\t%52.50f\nIntegral result = \t%52.50f\nIntegral error = \t%52.50f\nDuration is: %f seconds\n\n", N, M_PI, result, calc_error, duration.count());
	
	return duration;
}

chrono::duration<double> do_the_thing_openMP(uint32_t N) {
	double a = 0, b = 1;
	double h = (b - a) / (N);
	double sum = 0;
	double *xs = new double [N];
	double* fs = new double[N];
	
	xs[0] = a;
	for (int i = 1; i < N; i++)
		xs[i] = xs[i - 1] + h;

	for (int i = 0; i < N; i++)
		fs[i] = get_f(xs[i]);

	chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();

	double x = a;
/*#pragma omp parallel for
	for (int i = 2; i <= N - 2; i += 2, x = x + h + h)
		sum += 2 * get_f(x);

	x = a + h;
#pragma omp parallel for
	for (int i = 1; i <= N - 1; i += 2, x = x + h + h)
		sum += 4 * get_f(x);
*/
#pragma omp parallel for reduction (+:sum)
	for (int i = 1; i <= N - 1; i++) {
		//sum += ((i % 2) ? 4 : 2) * get_f(xs[i]);
		sum += ((i % 2) ? 4 : 2) * fs[i];
	}
#pragma omp barrier

	sum += get_f(a) + get_f(b);

	chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
	chrono::duration<double> duration = (t2 - t1);

	double result = h / 3 * sum;
	double calc_error = abs(result - M_PI);

	printf("N = %d\nIdeal result = \t\t%52.50f\nIntegral result = \t%52.50f\nIntegral error = \t%52.50f\nDuration is: %f seconds\n\n", N, M_PI, result, calc_error, duration.count());

	return duration;
}

chrono::duration<double> do_the_thing_no_vector(uint32_t N) {
	double a = 0, b = 1;
	double h = (b - a) / (N);
	double sum = 0;

	chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();

	sum += get_f(a) + get_f(b);

	double x = a + h + h;
#pragma loop(no_vector)
	for (int i = 2; i <= N - 2; i += 2, x = x + h + h)
		sum += 2 * get_f(x);

	x = a + h;
#pragma loop(no_vector)
	for (int i = 1; i <= N - 1; i += 2, x = x + h + h)
		sum += 4 * get_f(x);

	chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
	chrono::duration<double> duration = (t2 - t1);

	double result = h / 3 * sum;
	double calc_error = abs(result - M_PI);

	printf("N = %d\nIdeal result = \t\t%52.50f\nIntegral result = \t%52.50f\nIntegral error = \t%52.50f\nDuration is: %f seconds\n\n", N, M_PI, result, calc_error, duration.count());

	return duration;
}

chrono::duration<double> do_the_thing_qpar(uint32_t N) {
	double a = 0, b = 1;
	double h = (b - a) / (N);
	double sum = 0;

	chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();

	sum += get_f(a) + get_f(b);

	double x = a + h + h;
#pragma loop(hint_parallel(8))
	for (int i = 2; i <= N - 2; i += 2, x = x + h + h)
		sum += 2 * get_f(x);

	x = a + h;
#pragma loop(hint_parallel(8))
	for (int i = 1; i <= N - 1; i += 2, x = x + h + h)
		sum += 4 * get_f(x);

	chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
	chrono::duration<double> duration = (t2 - t1);

	double result = h / 3 * sum;
	double calc_error = abs(result - M_PI);

	printf("N = %d\nIdeal result = \t\t%52.50f\nIntegral result = \t%52.50f\nIntegral error = \t%52.50f\nDuration is: %f seconds\n\n", N, M_PI, result, calc_error, duration.count());

	return duration;
}

chrono::duration<double> do_the_thing_thread(uint32_t N) {
	double a = 0, b = 1;
	double h = (b - a) / (N);
	double sum = 0;

	chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();

	sum += get_f(a) + get_f(b);

	double dx = (b - a) / 8;

	double sum0 = 0, x0 = a;
	thread thread0([&sum0, h, &x0, N]() {
		x0 = x0 + h + h;
		for (int i = 2; i < (N * 1) / 8; i += 2, x0 = x0 + h + h) {
			sum0 += 2 * get_f(x0);
			sum0 += 4 * get_f(x0 - h);
		}
		sum0 += 4 * get_f(x0 - h);
		});

	double sum1 = 0, x1 = x0 + dx;
	thread thread1([&sum1, h, &x1, N]() {
		for (int i = (N * 1) / 8; i < (N * 2) / 8; i += 2, x1 = x1 + h + h) {
			sum1 += 2 * get_f(x1);
			sum1 += 4 * get_f(x1 - h);
		}
		sum1 += 4 * get_f(x1 - h);
		});

	double sum2 = 0, x2 = x1 + dx;
	thread thread2([&sum2, h, &x2, N]() {
		for (int i = (N * 2) / 8; i < (N * 3) / 8; i += 2, x2 = x2 + h + h){
			sum2 += 2 * get_f(x2);
			sum2 += 4 * get_f(x2 - h);
		}
		sum2 += 4 * get_f(x2 - h);
		});

	double sum3 = 0, x3 = x2 + dx;
	thread thread3([&sum3, h, &x3, N]() {
		for (int i = (N * 3) / 8; i < (N * 4) / 8; i += 2, x3 = x3 + h + h){
			sum3 += 2 * get_f(x3);
			sum3 += 4 * get_f(x3 - h);
		}
		sum3 += 4 * get_f(x3 - h);
		});

	double sum4 = 0, x4 = x3 + dx;
	thread thread4([&sum4, h, &x4, N]() {
		for (int i = (N * 4) / 8;  i < (N * 5) / 8; i += 2, x4 = x4 + h + h){
			sum4 += 2 * get_f(x4);
			sum4 += 4 * get_f(x4 - h);
		}
		sum4 += 4 * get_f(x4 - h);
		});

	double sum5 = 0, x5 = x4 + dx;
	thread thread5([&sum5, h, &x5, N]() {
		for (int i = (N * 5) / 8; i < (N * 6) / 8; i += 2, x5 = x5 + h + h){
			sum5 += 2 * get_f(x5);
			sum5 += 4 * get_f(x5 - h);
		}
		sum5 += 4 * get_f(x5 - h);
		});

	double sum6 = 0, x6 = x5 + dx;
	thread thread6([&sum6, h, &x6, N]() {
		for (int i = (N * 6) / 8; i < (N * 7) / 8; i += 2, x6 = x6 + h + h){
			sum6 += 2 * get_f(x6);
			sum6 += 4 * get_f(x6 - h);
		}
		sum6 += 4 * get_f(x6 - h);
		});

	double sum7 = 0, x7 = x6 + dx;
	thread thread7([&sum7, h, &x7, N]() {
		for (int i = (N * 7) / 8; i <= (N * 8) / 8 - 2; i += 2, x7 = x7 + h + h){
			sum7 += 2 * get_f(x7);
			sum7 += 4 * get_f(x7 - h);
		}
		sum7 += 4 * get_f(x7 - h);
		});

	thread0.join(); thread1.join(); thread2.join(); thread3.join();
	thread4.join(); thread5.join(); thread6.join(); thread7.join();

	sum = sum0 + sum1 + sum2 + sum3 + \
		  sum4 + sum5 + sum6 + sum7;

	chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
	chrono::duration<double> duration = (t2 - t1);

	double result = h * sum / 3;
	double calc_error = abs(result - M_PI);

	printf("N = %d\nIdeal result = \t\t%52.50f\nIntegral result = \t%52.50f\nIntegral error = \t%52.50f\nDuration is: %f seconds\n\n", N, M_PI, result, calc_error, duration.count());

	return duration;
}

#define BIG_N

int main() {
	printf("------------Usual integral calculation:------------\n");
	do_the_thing_usual(100);
	do_the_thing_usual(1000);
	do_the_thing_usual(10000);
	do_the_thing_usual(100000);
	do_the_thing_usual(1000000);
#ifdef BIG_N
	do_the_thing_usual(10000000);
	do_the_thing_usual(100000000);
	//do_the_thing_usual(1000000000);
#endif
//	printf("\n---------No vector integral calculation:---------\n");
//	do_the_thing_no_vector(100);
//	do_the_thing_no_vector(1000);
//	do_the_thing_no_vector(10000);
//	do_the_thing_no_vector(100000);
//	do_the_thing_no_vector(1000000);
//#ifdef BIG_N
//	do_the_thing_no_vector(10000000);
//	do_the_thing_no_vector(100000000);
//	do_the_thing_no_vector(1000000000);
//#endif
//	printf("\n------------QPar integral calculation:-----------\n");
//	do_the_thing_qpar(100);
//	do_the_thing_qpar(1000);
//	do_the_thing_qpar(10000);
//	do_the_thing_qpar(100000);
//	do_the_thing_qpar(1000000);
//#ifdef BIG_N
//	do_the_thing_qpar(10000000);
//	do_the_thing_qpar(100000000);
//	do_the_thing_qpar(1000000000);
//#endif
//	printf("\n----------Thread integral calculation:-----------\n");
//	do_the_thing_thread(100);
//	do_the_thing_thread(1000);
//	do_the_thing_thread(10000);
//	do_the_thing_thread(100000);
//	do_the_thing_thread(1000000);
//#ifdef BIG_N
//	do_the_thing_thread(10000000);
//	do_the_thing_thread(100000000);
//	do_the_thing_thread(1000000000);
//#endif
//	printf("------------OpenMP integral calculation:------------\n");
//	do_the_thing_openMP(100);
//	do_the_thing_openMP(1000);
//	do_the_thing_openMP(10000);
//	do_the_thing_openMP(100000);
//	do_the_thing_openMP(1000000);
//#ifdef BIG_N
//	do_the_thing_openMP(10000000);
//	do_the_thing_openMP(100000000);
//	do_the_thing_openMP(1000000000);
//#endif



}