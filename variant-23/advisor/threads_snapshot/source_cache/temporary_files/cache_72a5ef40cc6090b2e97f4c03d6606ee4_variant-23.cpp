// variant 23: integral(8 / (1+x^2), 0, 1) method of central rectangles.
// /arch:AVX2 
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <cmath>
#include <chrono>
#include <thread>
#include "advisor-annotate.h"

using namespace std;

constexpr auto MAX_ITERATIONS = 8;

double fun(double x) {
  return 8 / (1 + x * x);
}

//#define sequential
#define threads

#ifdef sequential

double calc_integral(size_t n) {
  const double a = 0;
  const double b = 1;
  double h = (b - a) / n;
  double h_2 = h / 2;
  double xi = a;
  double res = 0;
  ANNOTATE_SITE_BEGIN(seq_loop);
  ANNOTATE_ITERATION_TASK(seq_iter_task);
  for (size_t i = 0; i < n; i++) {
    ANNOTATE_TASK_BEGIN(seq_loop_body);
    res += fun(xi + h_2) * h;
    xi += h;
    ANNOTATE_TASK_END(seq_loop_body);
  }
  ANNOTATE_SITE_END(seq_loop);
  return res;
}

int main() {
  chrono::steady_clock::time_point tic, toc;
  const double realRes = 8 * atan(1);
  size_t n = 10;
  cout << "Integral 8/(1+x^2) 0..1 = 2*pi = " << realRes << endl;
  cout << "Sequential computation:\n";
  for (size_t i = 0; i < MAX_ITERATIONS; i++) {
    tic = chrono::steady_clock::now();
    double res = calc_integral(n);
    toc = chrono::steady_clock::now();

    cout << "Result for n = " << n << "\t is " << res <<
      "\tprecision = " << realRes - res << ", \telapsed time = " <<
      (chrono::duration_cast<chrono::nanoseconds>(toc - tic).count()) / 1.0e+9 << " seconds." << endl;
    n *= 10;
  }
}

#endif

#ifdef novector

double calc_integral(size_t n) {
  const double a = 0;
  const double b = 1;
  double h = (b - a) / n;
  double h_2 = h / 2;
  double xi = a;
  double res = 0;

  for (size_t i = 0; i < n; i++) {
    res += fun(xi + h_2) * h;
    xi += h;
  }
  return res;
}

int main() {
  chrono::steady_clock::time_point tic, toc;
  const double realRes = 8 * atan(1);
  size_t n = 10;
  cout << "Integral 8/(1+x^2) 0..1 = 2*pi = " << realRes << endl;
  cout << "Sequential computation:\n";
  for (size_t i = 0; i < MAX_ITERATIONS; i++) {
    tic = chrono::steady_clock::now();
    double res = calc_integral(n);
    toc = chrono::steady_clock::now();

    cout << "Result for n = " << n << "\t is " << res <<
      "\tprecision = " << realRes - res << ", \telapsed time = " <<
      (chrono::duration_cast<chrono::nanoseconds>(toc - tic).count()) / 1.0e+9 << " seconds." << endl;
    n *= 10;
  }
  n = 10;
  cout << endl;

  cout << "Pragma loop(no_vector) computation:\n";
#pragma loop(no_vector)
  for (size_t i = 0; i < MAX_ITERATIONS; i++) {
    tic = chrono::steady_clock::now();
    double res = calc_integral(n);
    toc = chrono::steady_clock::now();

    cout << "Result for n = " << n << "\t is " << res <<
      "\tprecision = " << realRes - res << ", \telapsed time = " <<
      (chrono::duration_cast<chrono::nanoseconds>(toc - tic).count()) / 1.0e+9 << " seconds." << endl;
    n *= 10;
  }
  n = 10;
  cout << endl;
}

#endif

#ifdef hintparallel

double calc_integral(size_t n) {
  const double a = 0;
  const double b = 1;
  double h = (b - a) / n;
  double h_2 = h / 2;
  double xi = a;
  double res = 0;
#pragma loop(hint_parallel(8))
  for (size_t i = 0; i < n; i++) {
    res += fun(xi + h_2) * h;
    xi += h;
  }
  return res;
}

int main() {
  chrono::steady_clock::time_point tic, toc;
  const double realRes = 8 * atan(1);
  size_t n = 10;
  cout << "Integral 8/(1+x^2) 0..1 = 2*pi = " << realRes << endl;
  cout << "Pragma loop(hint_parallel(8)) computation:\n";
#pragma loop(hint_parallel(8))
  for (size_t i = 0; i < MAX_ITERATIONS; i++) {
    tic = chrono::steady_clock::now();
    double res = calc_integral(n);
    toc = chrono::steady_clock::now();

    cout << "Result for n = " << n << "\t is " << res <<
      "\tprecision = " << realRes - res << ", \telapsed time = " <<
      (chrono::duration_cast<chrono::nanoseconds>(toc - tic).count()) / 1.0e+9 << " seconds." << endl;
    n *= 10;
  }
}

#endif 

#ifdef threads

void calc_integral(double& res, size_t n, double h, double h_2, double xi) {
  ANNOTATE_SITE_BEGIN(thread_loop);
  ANNOTATE_ITERATION_TASK(iter_task);
  for (size_t i = 0; i < n; i++) {
    ANNOTATE_TASK_BEGIN(loop_task);
    res += fun(xi + h_2) * h;
    xi += h; 
    ANNOTATE_TASK_END(loop_task);
  }
  ANNOTATE_SITE_END(thread_loop);
}

int main() {
  const double a = 0;
  const double b = 1;
  chrono::steady_clock::time_point tic, toc;
  const double realRes = 8 * atan(1);
  size_t n = 10;
  cout << "Integral 8/(1+x^2) 0..1 = 2*pi = " << realRes << endl;
  cout << "Thread computation:\n";
  for (size_t i = 0; i < MAX_ITERATIONS; i++) {
    size_t n_div = n / 8;
    double h = (b - a) / n;
    double h_2 = h / 2;
    double xi = a;
    double res = 0;
    double res0 = 0;
    double res1 = 0;
    double res2 = 0;
    double res3 = 0;
    double res4 = 0;
    double res5 = 0;
    double res6 = 0;
    double res7 = 0;
    thread tread0(calc_integral, ref(res0), n_div, h, h_2, xi);
    xi += h * n_div;
    thread tread1(calc_integral, ref(res1), n_div, h, h_2, xi);
    xi += h * n_div;
    thread tread2(calc_integral, ref(res2), n_div, h, h_2, xi);
    xi += h * n_div;
    thread tread3(calc_integral, ref(res3), n_div, h, h_2, xi);
    xi += h * n_div;
    thread tread4(calc_integral, ref(res4), n_div, h, h_2, xi);
    xi += h * n_div;
    thread tread5(calc_integral, ref(res5), n_div, h, h_2, xi);
    xi += h * n_div;
    thread tread6(calc_integral, ref(res6), n_div, h, h_2, xi);
    xi += h * n_div;
    thread tread7(calc_integral, ref(res7), n_div + n % 8, h, h_2, xi);
    tic = chrono::steady_clock::now();
    tread0.join();
    tread1.join();
    tread2.join();
    tread3.join();
    tread4.join();
    tread5.join();
    tread6.join();
    tread7.join();
    res = res0 + res1 + res2 + res3 + res4 + res5 + res6 + res7;
    toc = chrono::steady_clock::now();

    cout << "Result for n = " << n << "\t is " << res <<
      "\tprecision = " << realRes - res << ", \telapsed time = " <<
      (chrono::duration_cast<chrono::nanoseconds>(toc - tic).count()) / 1.0e+9 << " seconds." << endl;
    n *= 10;
  }
}

#endif // threads